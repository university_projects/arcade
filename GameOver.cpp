#include "GameOver.hpp"
#include "Matrix.hpp"
#include "Notes.h"
#include "LiquidCrystal.h"

extern Matrix matrix;
extern LiquidCrystal lcd;

////////////////////////////////////////////////
GameOver::GameOver(bool gameWin)
{
	matrix.clear(true);

	/// Left eye
	matrix [2] [12] = true;
	matrix [2] [11] = true;
	matrix [2] [10] = true;

	/// Right eye
	matrix [5] [12] = true;
	matrix [5] [11] = true;
	matrix [5] [10] = true;

	if (gameWin)
	{
		/// The smile
		matrix [1] [7] = true;
		matrix [1] [6] = true;
		matrix [2] [5] = true;
		matrix [3] [4] = true;
		matrix [4] [4] = true;
		matrix [5] [5] = true;
		matrix [6] [6] = true;
		matrix [6] [7] = true;

		/// The super mario level complete song :D
		melody.setSize(27);

		int notes [] =
		{
			NOTE_G3, NOTE_C4, NOTE_E4, NOTE_G4, NOTE_C5, NOTE_E5, NOTE_G5, NOTE_E5,
			NOTE_GS3, NOTE_C4, NOTE_DS4, NOTE_GS4, NOTE_C5, NOTE_DS5, NOTE_GS5, NOTE_DS5,
			NOTE_AS3, NOTE_D4, NOTE_F4, NOTE_AS4, NOTE_D5, NOTE_F5, NOTE_AS5, NOTE_AS5, NOTE_AS5, NOTE_AS5, NOTE_C6
		};

		int notesDuration [] =
		{
			8, 8, 8, 8, 8, 8, 4, 4,
			8, 8, 8, 8, 8, 8, 4, 4,
			8, 8, 8, 8, 8, 8, 4, 8, 8, 8, 2
		};

		for (int i = 0; i < melody.getSize(); i++)
		{
			Note *note = melody [i];
			note->note = notes [i];
			note->duration = notesDuration [i];
		}

		lcd.setCursor(0, 0);
		lcd.print("Felicidades");
		lcd.setCursor(0, 1);
		lcd.print("Ganaste :D!!");
	}
	else
	{
		/// A sad mouth
		matrix [1] [4] = true;
		matrix [2] [5] = true;
		matrix [3] [6] = true;
		matrix [4] [6] = true;
		matrix [5] [5] = true;
		matrix [6] [4] = true;

		lcd.setCursor(0, 0);
		lcd.print("Vaya");
		lcd.setCursor(0, 1);
		lcd.print("Perdiste :(");
	}

	matrix.print();
}

////////////////////////////////////////////////
void GameOver::update(unsigned int deltaTime)
{
	melody.update(deltaTime);
	if (melody.getCurrentNoteIndex() == melody.getSize() - 1)
		running = false;
}
