#include "Piece.hpp"
#include "Arduino.h"

////////////////////////////////////////////////
void Piece::generate()
{
	type = Type(random(0, 6));

	switch (type)
	{
		case I:
			leds [0].x = 4;
			leds [0].y = 14;

			leds [1].x = 4;
			leds [1].y = 15;

			leds [2].x = 4;
			leds [2].y = 13;

			leds [3].x = 4;
			leds [3].y = 12;

			horizontal = false;
		break;

		case O:
			leds [0].x = 3;
			leds [0].y = 15;

			leds [1].x = 4;
			leds [1].y = 15;

			leds [2].x = 3;
			leds [2].y = 14;

			leds [3].x = 4;
			leds [3].y = 14;
		break;

		case L:
			leds [0].x = 3;
			leds [0].y = 14;

			leds [1].x = 3;
			leds [1].y = 15;

			leds [2].x = 3;
			leds [2].y = 13;

			leds [3].x = 4;
			leds [3].y = 13;
		break;

		case T:
			leds [0].x = 3;
			leds [0].y = 14;

			leds [1].x = 2;
			leds [1].y = 14;

			leds [2].x = 4;
			leds [2].y = 14;

			leds [3].x = 3;
			leds [3].y = 13;
		break;

		case S:
			leds [0].x = 3;
			leds [0].y = 14;

			leds [1].x = 2;
			leds [1].y = 13;

			leds [2].x = 3;
			leds [2].y = 13;

			leds [3].x = 4;
			leds [3].y = 14;
		break;

		case Z:
			leds [0].x = 3;
			leds [0].y = 14;

			leds [1].x = 2;
			leds [1].y = 14;

			leds [2].x = 3;
			leds [2].y = 13;

			leds [3].x = 4;
			leds [3].y = 13;
		break;

		case J:
			leds [0].x = 3;
			leds [0].y = 14;

			leds [1].x = 3;
			leds [1].y = 15;

			leds [2].x = 3;
			leds [2].y = 13;

			leds [3].x = 2;
			leds [3].y = 13;
	}
}

////////////////////////////////////////////////
void Piece::move(Direction direction)
{
	switch (direction)
	{
		case Left:
			for (int i = 0; i < 4; i++)
				leds [i].x--;
		break;

		case Right:
			for (int i = 0; i < 4; i++)
				leds [i].x++;
		break;

		case Up:
			for (int i = 0; i < 4; i++)
				leds [i].y++;
		break;

		case Down:
			for (int i = 0; i < 4; i++)
				leds [i].y--;
	}
}

////////////////////////////////////////////////
void Piece::rotate()
{
	if (type == O)
		return;

	const Led &origin = leds [0];

	if (type == I)
	{
		horizontal = ! horizontal;

		if (horizontal)
		{
			leds [1].x = origin.x + 1;
			leds [1].y = origin.y;

			leds [2].x = origin.x - 1;
			leds [2].y = origin.y;

			leds [3].x = origin.x - 2;
			leds [3].y = origin.y;
		}
		else
		{
			leds [1].x = origin.x;
			leds [1].y = origin.y + 1;

			leds [2].x = origin.x;
			leds [2].y = origin.y - 1;

			leds [3].x = origin.x;
			leds [3].y = origin.y - 2;
		}

		return;
	}

	for (int i = 1; i < 4; i++)
	{
		Led &led = leds [i];

		if (led.x < origin.x)
		{
			if (led.y < origin.y)
				led.y += 2;
			else if (led.y == origin.y)
			{
				led.x++;
				led.y++;
			}
			else
				led.x += 2;
		}

		else if (led.x == origin.x)
		{
			if (led.y > origin.y)
			{
				led.x++;
				led.y--;
			}
			else
			{
				led.x--;
				led.y++;
			}
		}
		else
		{
			if (led.y < origin.y)
				led.x -= 2;
			else if (led.y == origin.y)
			{
				led.x--;
				led.y--;
			}
			else
			{
				led.y -= 2;
			}
		}
	}
}
