#ifndef GAMEPAD_HPP
#define GAMEPAD_HPP

class Gamepad
{
public:
	///// The value of the enum is the arduino's pin where it must be connected
	enum Key
	{
		Up	 	= 3,
		Down	= 4,
		Left	= 2,
		Right	= 5,
		A		= 6
	};

	enum KeyState
	{
		Normal,
		Pressed,
		Released
	};

	Gamepad();
	bool isKeyPressed(Key key) const;
	bool wasKeyReleased(Key key) const;
	void update();

private:
	KeyState upState;
	KeyState downState;
	KeyState rightState;
	KeyState leftState;
	KeyState aState;
};

#endif // GAMEPAD_HPP
