#ifndef MATRIX_DRIVER_HPP
#define MATRIX_DRIVER_HPP

#include "LedControl.h"

#define COLUMNS 8
#define ROWS	16

class Matrix
{
public:
	Matrix();
	~Matrix();
	void setup();
	void clear(bool clearLogic = false);
	void print();

	bool * operator[](unsigned int column);

private:
	bool matrix [8] [16];
	LedControl *ledControl;
};

#endif // MATRIX_DRIVER_HPP
