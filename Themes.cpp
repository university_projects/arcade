#include "Themes.hpp"
#include "Notes.h"

////////////////////////////////////////////////
void initMainTheme(Melody &melody)
{
	int melodySize = melody.getSize();
	int melodyAux [melodySize] =
	{
		NOTE_E4, NOTE_E4, NOTE_E4, NOTE_E4, 0, NOTE_C4, NOTE_E4, NOTE_G4, 0, NOTE_G3,         // 10 Part1
		NOTE_C4, 0, NOTE_G3, 0, NOTE_E3, 0,                                                   // 6  Part2
		NOTE_A3, NOTE_B3, NOTE_AS3, NOTE_A3, NOTE_G3,                                         // 5  Part3
		NOTE_E4, NOTE_G4, NOTE_A4, NOTE_F4, NOTE_G4, NOTE_E4, NOTE_C4, NOTE_D4, NOTE_B3,      // 9  Part4
		NOTE_C4, 0, NOTE_G3, 0, NOTE_E3, 0,                                                   // 6  Part2
		NOTE_A3, NOTE_B3, NOTE_AS3, NOTE_A3, NOTE_G3,                                         // 5  Part3
		NOTE_E4, NOTE_G4, NOTE_A4, NOTE_F4, NOTE_G4, NOTE_E4, NOTE_C4, NOTE_D4, NOTE_B3,      // 9  Part4
		NOTE_G4, NOTE_FS4, NOTE_F4, NOTE_DS4, NOTE_E4,                                        // 5  Part5
		NOTE_GS3, NOTE_A3, NOTE_C4, NOTE_A3, NOTE_C4, NOTE_D4,                                // 6  Part6
		NOTE_G4, NOTE_FS4, NOTE_F4, NOTE_DS4, NOTE_E4,                                        // 5  Part5
		NOTE_C5, NOTE_C5, NOTE_C5,                                                            // 3  Part7
		NOTE_G4, NOTE_FS4, NOTE_F4, NOTE_DS4, NOTE_E4,                                        // 5  Part5
		NOTE_GS3, NOTE_A3, NOTE_C4, NOTE_A3, NOTE_C4, NOTE_D4,                                // 6  Part6
		NOTE_DS4, 0, NOTE_D4, 0, NOTE_C4,                                                     // 5  Part8
		NOTE_C4, NOTE_C4, 0, NOTE_C4, 0, NOTE_C4, NOTE_D4, NOTE_E4,                           // 8  Part9
		NOTE_C4, NOTE_A3, NOTE_G3,                                                            // 3  Part10
		NOTE_C4, NOTE_C4, 0, NOTE_C4, 0, NOTE_C4, NOTE_D4, NOTE_E4,                           // 8  Part9
		0,                                                                                    // 1  Part11
		NOTE_C4, NOTE_C4, 0, NOTE_C4, 0, NOTE_C4, NOTE_D4, NOTE_E4,                           // 8  Part9
		NOTE_C4, NOTE_A3, NOTE_G3,                                                            // 3  Part10
	};

	int notesDurationAux [melodySize] =
	{
		8, 8, 8, 8, 8, 8, 4, 4, 4, 2,   // Part1
		4, 8, 4, 8, 4, 8,               // Part2
		4, 4, 8, 4, 4,                  // Part3
		8, 8, 4, 8, 4, 4, 8, 8, 2,      // Part4
		4, 8, 4, 8, 4, 8,               // Part2
		4, 4, 8, 4, 4,                  // Part3
		8, 8, 4, 8, 4, 4, 8, 8, 2,      // Part4
		8, 8, 8, 4, 4,                  // Part5
		8, 8, 4, 8, 8, 2,               // Part6
		8, 8, 8, 4, 4,                  // Part5
		4, 8, 2,                        // Part7
		8, 8, 8, 4, 4,                  // Part5
		8, 8, 4, 8, 8, 2,               // Part6
		4, 8, 4, 8, 2,                  // Part8
		8, 8, 8, 8, 8, 8, 4, 8,         // Part9
		4, 8, 2,                        // Part10
		8, 8, 8, 8, 8, 8, 4, 8,         // Part9
		2,                              // Part11
		8, 8, 8, 8, 8, 8, 4, 8,         // Part9
		4, 8, 2,                        // Part10
	};

	for (int i = 0; i < melodySize; i++)
	{
		Note *note = melody [i];
		note->note = melodyAux [i];
		note->duration = notesDurationAux [i];
	}
}
