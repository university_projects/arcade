#ifndef GAME_OVER_HPP
#define GAME_OVER_HPP

#include "Game.hpp"
#include "Melody.hpp"

class GameOver : public Game
{
public:
	GameOver(bool gameWin);
	void update(unsigned int deltaTime) override;

private:
	Melody melody;
};

#endif // GAME_OVER_HPP
