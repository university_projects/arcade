#ifndef THEMES_HPP
#define THEMES_HPP

#include "Melody.hpp"

void initMainTheme(Melody &melody);

#endif // THEMES_HPP
