#include "Melody.hpp"
#include "Arduino.h"

////////////////////////////////////////////////
Melody::Melody(int size, int speakerPin) : size(0), speakerPin(speakerPin), currentNote(-1), currentTimeBetweenNotes(0),
										   pauseBetweenNotes(0)
{
	setSize(size);
}

////////////////////////////////////////////////
Melody::~Melody()
{
	delete [] notes;
}

////////////////////////////////////////////////
Note * Melody::operator[](int index)
{
	return &notes [index];
}

////////////////////////////////////////////////
void Melody::update(unsigned int deltaTime)
{
	currentTimeBetweenNotes += deltaTime;

	if (currentTimeBetweenNotes >= pauseBetweenNotes)
	{
		noTone(speakerPin);

		currentNote++;
		if (currentNote >= size)
			currentNote = 0;

		Note &note = notes [currentNote];
		int noteDuration = 1000 / note.duration;
		tone(speakerPin, note.note, noteDuration);

		pauseBetweenNotes = noteDuration * 1.3;
		currentTimeBetweenNotes = 0;
	}
}

////////////////////////////////////////////////
void Melody::setSize(int size)
{
	if (this->size)
		return;

	this->size = size;
	notes = new Note [size];
}

////////////////////////////////////////////////
int Melody::getSize() const
{
	return size;
}

////////////////////////////////////////////////
int Melody::getCurrentNoteIndex() const
{
	return currentNote;
}
