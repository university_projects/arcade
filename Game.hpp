#ifndef GAME_HPP
#define GAME_HPP

#define COLUMNS 8
#define ROWS	16

enum class GameType
{
	Tetris,
	Snake
};

class Game
{
public:
	Game() : running(true), win(false) {}
	virtual ~Game() {}
	virtual void update(unsigned int deltaTime) = 0;

	bool running;
	bool win;
};

#endif // GAME_HPP
