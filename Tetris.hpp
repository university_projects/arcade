#ifndef TETRIS_HPP
#define TETRIS_HPP

#include "Piece.hpp"
#include "Game.hpp"

class Tetris : public Game
{
public:
	Tetris();
	bool rotate();
	bool move (Direction direction);
	void step (unsigned int deltaTime);
	void update(unsigned int deltaTime) override;

private:
	void writeInMatrix();
	void clearInMatrix ();
	bool checkAvailability(Piece piece);

	int level;
	int score;
	Piece piece;
	Piece clone;
	unsigned int stepTime;
	int currentClearedRows;
	unsigned int currentTime;
};

#endif // TETRIS_HPP
