#include "LCD.hpp"
#include "LiquidCrystal.h"

extern LiquidCrystal lcd;

void lcd_printLevelScore(int level, int score)
{
	lcd.clear();

	lcd.setCursor(0, 0);
	lcd.print("Nivel");
	lcd.setCursor(9, 0);
	lcd.print(level);

	lcd.setCursor(0, 1);
	lcd.print("Puntos");
	lcd.setCursor(9, 1);
	lcd.print(score);
}
