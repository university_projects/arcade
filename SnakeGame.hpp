#ifndef SNAKE_GAME_HPP
#define SNAKE_GAME_HPP

#include "Game.hpp"
#include "Snake.hpp"

class SnakeGame : public Game
{
public:
	SnakeGame();
	void update(unsigned int deltaTime) override;

private:
	void generateFood();
	void gameOver();

	float stepTime;
	float currentTime;
	Snake snake;
	Led food;

	int score;
	int level;
};

#endif // SNAKE_GAME_HPP
