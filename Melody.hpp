#ifndef MELODY_HPP
#define MELODY_HPP

struct Note
{
	int note;
	int duration;
};

class Melody
{
public:
	Melody(int size = 0, int speakerPin = 13);
	~Melody();

	Note * operator[](int index);
	void update(unsigned int deltaTime);
	void setSize(int size);
	int getSize() const;
	int getCurrentNoteIndex() const;

private:
	int size;
	Note *notes;
	int currentNote;
	int pauseBetweenNotes;
	int currentTimeBetweenNotes;
	int speakerPin;
};

#endif // MELODY_HPP
