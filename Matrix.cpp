#include "Matrix.hpp"

////////////////////////////////////////////////
Matrix::Matrix()
{
	ledControl = new LedControl(8, 10, 9, 2);

	/// Init the matrix
	for (int row = 0; row < ROWS; row++)
		for (int column = 0; column < COLUMNS; column++)
			matrix [column] [row] = false;
}

////////////////////////////////////////////////
Matrix::~Matrix()
{
	delete ledControl;
}

////////////////////////////////////////////////
void Matrix::setup()
{
	ledControl->shutdown(0, false);
	ledControl->setIntensity(0, 0);

	ledControl->shutdown(1, false);
	ledControl->setIntensity(1, 0);
}

////////////////////////////////////////////////
void Matrix::clear(bool clearLogic)
{
	if (clearLogic)
		for (int row = 0; row < ROWS; row++)
			for (int column = 0; column < COLUMNS; column++)
				matrix [column] [row] = false;

	ledControl->clearDisplay(0);
	ledControl->clearDisplay(1);
}

////////////////////////////////////////////////
void Matrix::print()
{
	//// Print in the matrix at 1 address
	for (int row = 7; row >= 0; row--)
		for (int column = 0; column < COLUMNS; column++)
			if (matrix [column] [row])
				ledControl->setLed(0, 7 - row, column, true);

	//// Print in the matrix at 1 address
	for (int row = ROWS - 1; row >= 8; row--)
		for (int column = 0; column < COLUMNS; column++)
			if (matrix [column] [row])
				ledControl->setLed(1, row - 8, 7 - column, true);
}

////////////////////////////////////////////////
bool * Matrix::operator[](unsigned int column)
{
	return matrix [column];
}
