#include "Game.hpp"
#include "Menu.hpp"
#include "Tetris.hpp"
#include "Matrix.hpp"
#include "Gamepad.hpp"
#include "SnakeGame.hpp"
#include "GameOver.hpp"
#include "Melody.hpp"
#include "Themes.hpp"
#include "LiquidCrystal.h"

Gamepad gamepad;
Matrix matrix;
LiquidCrystal lcd(A0, A1, A2, A3, A4, A5);

void setup()
{
	randomSeed(analogRead(0));
	pinMode(8, OUTPUT);
	matrix.setup();
	lcd.begin(16, 2);
}

void loop()
{
	Menu *menu = new Menu;
	Melody *mainTheme = new Melody(116); // Super mario bros song :)
	initMainTheme(*mainTheme);

	unsigned long lastTime = millis();
	unsigned long currentTime = 0;
	while (menu->running)
	{
		currentTime = millis();
		unsigned long deltaTime = currentTime - lastTime;
		lastTime = currentTime;

		gamepad.update();
		menu->update(deltaTime);
		mainTheme->update(deltaTime);
	}

	matrix.clear(true);
	Game *game;
	switch (menu->getSelectedGame())
	{
		case GameType::Tetris: delete menu; game = new Tetris; break;
		case GameType::Snake: delete menu; game = new SnakeGame;
	}

	lastTime = millis();
	currentTime = 0;
	unsigned long currentFrameTime = 0;
	const unsigned long frameTime = 1000 / 10;
	while (game->running)
	{
		gamepad.update();

		currentTime = millis();
		unsigned long deltaTime = currentTime - lastTime;
		currentFrameTime += deltaTime;
		lastTime = currentTime;

		game->update(deltaTime);
		mainTheme->update(deltaTime);

		if (currentFrameTime >= frameTime)
		{
			matrix.clear();
			matrix.print();
		}
	}

	delete mainTheme;

	GameOver gameOverScene(game->win);
	delete game;

	lastTime = millis();
	currentTime = 0;
	while (gameOverScene.running)
	{
		currentTime = millis();
		unsigned long deltaTime = currentTime - lastTime;
		lastTime = currentTime;

		gameOverScene.update(deltaTime);
	}
}
