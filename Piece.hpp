#ifndef PIECE_HPP
#define PIECE_HPP

#include "Led.hpp"
#include "Direction.hpp"

struct Piece
{
	enum Type
	{
		I = 0,
		O,
		J,
		L,
		S,
		Z,
		T
	};

	Led leds [4];
	Type type;
	bool horizontal;

	void generate();
	void move(Direction direction);
	void rotate();
};

#endif // PIECE_HPP
