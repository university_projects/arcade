#ifndef SNAKE_HPP
#define SNAKE_HPP

#include "Led.hpp"
#include "Direction.hpp"

struct SnakePiece : public Led
{
	SnakePiece *prev;
	SnakePiece *next;

	SnakePiece(UChar x = 0, UChar y = 0) : Led(x, y), prev(nullptr), next(nullptr) {}
};

class Snake
{
public:
	Snake();
	~Snake();

	void setDirection(Direction direction);
	void move();
	void eat();

	int getSize() const;
	const SnakePiece * operator[](int index) const;

private:
	int size;
	Direction currentDirection;
	Direction newDirection;
	bool needNewPiece;

	SnakePiece *head;
	SnakePiece *tail;
};

#endif // SNAKE_HPP
