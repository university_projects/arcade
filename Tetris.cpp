#include "Tetris.hpp"
#include "Gamepad.hpp"
#include "Matrix.hpp"
#include "LCD.hpp"

extern Gamepad gamepad;
extern Matrix matrix;

////////////////////////////////////////////////
Tetris::Tetris() : level(1), currentClearedRows(0), score(0), currentTime(0), stepTime(800)
{
	piece.generate();
	writeInMatrix();
}

////////////////////////////////////////////////
void Tetris::writeInMatrix()
{
	for (int i = 0; i < 4; i++)
		matrix [(int) piece.leds [i].x] [(int) piece.leds [i].y] = true;
}

////////////////////////////////////////////////
void Tetris::clearInMatrix()
{
	for (int i = 0; i < 4; i++)
		matrix [(int) piece.leds [i].x] [(int) piece.leds [i].y] = false;
}

////////////////////////////////////////////////
bool Tetris::checkAvailability(Piece piece)
{
	for (int i = 0; i < 4; i++)
	{
		if (piece.leds [i].x == -1 || piece.leds [i].x == COLUMNS ||
			piece.leds [i].y == -1 ||
			matrix [(int) piece.leds [i].x] [(int) piece.leds [i].y])
			return false;
	}
	return true;
}

////////////////////////////////////////////////
bool Tetris::rotate()
{
	clearInMatrix();

	clone = piece;
	clone.rotate();

	if (checkAvailability(clone))
		piece.rotate();

	writeInMatrix();
}

////////////////////////////////////////////////
void Tetris::update(unsigned int deltaTime)
{
	step (deltaTime);

	if (gamepad.wasKeyReleased(Gamepad::A))
		rotate();

	if (gamepad.wasKeyReleased(Gamepad::Down))
		move(Direction::Down);

	if (gamepad.wasKeyReleased(Gamepad::Left))
		move(Direction::Left);

	if (gamepad.wasKeyReleased(Gamepad::Right))
		move(Direction::Right);
}

///////////////////////////////////////////////
bool Tetris::move(Direction direction)
{
	clearInMatrix();
	piece.move(direction);

	if (!checkAvailability(piece))
	{
		piece.move(Direction(-(int)direction));
		writeInMatrix();
		return false;
	}

	writeInMatrix();
	return true;
}

///////////////////////////////////////////////
void Tetris::step (unsigned int deltaTime)
{
	currentTime += deltaTime;

	if (currentTime >= stepTime)
	{
		if (!move (Down))
		{
			piece.generate();
			int clearedRows = 0;

			if (!checkAvailability(piece))
				running = false;

			for (int i = 0; i < ROWS ;)
			{
				bool isRowFilled = true;

				for (int j = 0; j < COLUMNS ; j++)
				{
					if (!matrix [j][i])
					{
						isRowFilled = false;

						clearedRows++;

						currentClearedRows++;
						break;
					}
				}

				if (isRowFilled)
				{
					for (int j = 0; j < COLUMNS ; j++)
						matrix [j][i] = false;

					for (int row = i; row < (ROWS - 1) ; row++)
					{
						for (int col = 0 ; col < COLUMNS; col++)
						{
							matrix [col][row] = matrix [col] [row + 1];
						}
					}
				}
				else
					i++;

				switch (clearedRows)
				{
					case 1: score += level; break;
					case 2: score += (3 * level); break;
					case 3: score += (5 * level); break;
					case 4: score += (10 * level);
				}

				if (currentClearedRows >= 4)
				{
					level ++;
					stepTime -= 50;
					currentClearedRows -= 4;
				}

				lcd_printLevelScore(level, score);
			}
		}
		currentTime = 0;
	}
}
