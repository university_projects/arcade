#include "SnakeGame.hpp"
#include "Matrix.hpp"
#include "Gamepad.hpp"
#include "Arduino.h"
#include "LCD.hpp"

extern Matrix matrix;
extern Gamepad gamepad;

////////////////////////////////////////////////
SnakeGame::SnakeGame() : stepTime(500), currentTime(0), score(0), level(1)
{
	/// Write the snake on the matrix
	for (int i = 0; i < snake.getSize(); i++)
		matrix [snake [i]->x] [snake [i]->y] = true;

	generateFood();

	lcd_printLevelScore(level, score);
}

////////////////////////////////////////////////
void SnakeGame::update(unsigned int deltaTime)
{
	currentTime += deltaTime;

	if (gamepad.isKeyPressed(Gamepad::Up))
		snake.setDirection(Direction::Up);

	if (gamepad.isKeyPressed(Gamepad::Down))
		snake.setDirection(Direction::Down);

	if (gamepad.isKeyPressed(Gamepad::Right))
		snake.setDirection(Direction::Right);

	if (gamepad.isKeyPressed(Gamepad::Left))
		snake.setDirection(Direction::Left);

	if (currentTime >= stepTime)
	{
		/// Clear the snake from the matrix
		for (int i = 0; i < snake.getSize(); i++)
			matrix [snake [i]->x] [snake [i]->y] = false;

		snake.move();
		const SnakePiece *head = snake [0];

		/// Check that the snake doesn't get out of the matrix. If gets out lose
		if (head->x < 0 || head->x == COLUMNS || head->y < 0 || head->y == ROWS)
		{
			gameOver();
		}
		else
		{
			bool needGenerateFood = false;

			/// Checks if the snake ate the food
			if (head->x == food.x && head->y == food.y)
			{
				snake.eat();
				needGenerateFood = true;
				matrix [food.x] [food.y] = false;

				// Increase the score
				score += level;

				if (snake.getSize() == 3 * level)
				{
					level++;
					stepTime -= 50;
				}

				lcd_printLevelScore(level, score);
			}

			/// Write the snake's body on the matrix
			for (int i = 1; i < snake.getSize(); i++)
				matrix [snake [i]->x] [snake [i]->y] = true;

			/// Check that the snake doesn't collide with its body
			if (matrix [head->x] [head->y])
			{
				gameOver();
			}
			else
			{
				matrix [head->x] [head->y] = true;

				if (needGenerateFood)
					generateFood();

				matrix [food.x] [food.y] = true;
			}
		}

		currentTime = 0;
	}
}

////////////////////////////////////////////////
void SnakeGame::generateFood()
{
	do
	{
		food.x = random(0, COLUMNS - 1);
		food.y = random(0, ROWS - 1);
	} while (matrix [food.x] [food.y]);
}

////////////////////////////////////////////////
void SnakeGame::gameOver()
{
	matrix [food.x] [food.y] = false;

	for (int i = 1; i < snake.getSize(); i++)
		matrix [snake [i]->x] [snake [i]->y] = false;

	running = false;
}
